// ==UserScript==
// @name         Secretly Subscribe to PewDiePie
// @namespace    https://bitbucket.org/ttriisa/
// @version      1
// @downloadURL  http://www.z-bit.eu/s/sub2pew.user.js
// @updateURL    http://www.z-bit.eu/s/sub2pew.user.js
// @description  Subscribes to PewDiePie and hides subscription status.
// @include      /^https?:\/\/www.youtube.com/
// @author       ApDea
// ==/UserScript==

window.addEventListener("load", run);
document.body.addEventListener("yt-page-data-updated", function() {setTimeout(run, 100)});

console.log("Ahh, I'm just looking around here, nothing to worry about 🙄");

function run() {
	
	hideMyTraces();

	//Detect if we are on channel page
	let channelTitleElement = document.querySelector("#channel-title,#owner-name.ytd-video-owner-renderer");
	if(channelTitleElement && channelTitleElement.textContent.indexOf("PewDiePie") !== -1) {
		console.log("Found Gloria Borger");
		let subBtns = document.querySelectorAll("#subscribe-button");
		//Find the right subscribe button
		for(let i=0, subBtn; subBtn = subBtns[i]; i++) {
			let preElem = subBtn.previousElementSibling;
			if((preElem.tagName === "A" && preElem.href.indexOf("/user/PewDiePie") !== -1) || preElem.id === "edit-buttons" || preElem.textContent.indexOf("PewDiePie") !== -1) {
				if(subBtn.textContent.indexOf("SUBSCRIBE") !== -1) {
					console.log("Everything seems ok.");
				} else if(subBtn.textContent.indexOf("Subscribed") !== -1 || subBtn.textContent.indexOf("Tellitud") !== -1) {
					console.log("Everything seems ok. Just tweaking that button a little...");
					//Make it look like an non-subed button for disguise
					let buttons = subBtn.querySelectorAll('.style-scope.ytd-subscribe-button-renderer');
					buttons[0].style.backgroundColor = "var(--yt-spec-brand-button-background)";
					buttons[0].style.color = "var(--yt-subscribe-button-text-color)";
					buttons[1].textContent = "SUBSCRIBE";
					document.getElementById("notification-preference-toggle-button").style.display = "none";
				} else {
					//First sub then...
					console.log("Not subscribed? Fixing it right away!");
					let buttons = subBtn.querySelectorAll('.style-scope.ytd-subscribe-button-renderer');
					eventFire(buttons[0], "click");
					setTimeout(function() {
						//... make it look like an non-subed button for disguise
						buttons[0].style.backgroundColor = "var(--yt-spec-brand-button-background)";
						buttons[0].style.color = "var(--yt-subscribe-button-text-color)";
						buttons[1].textContent = "SUBSCRIBE";
						document.getElementById("notification-preference-toggle-button").style.display = "none";
						subBtn.style.pointerEvents = "none";
						hideMyTraces();
						console.log("Made that button unclickable too 🤣");
						console.log("Guess I'm going to jail...");
						setTimeout(hideMyTraces, 300);
					}, 300);
				}
			}
		}
	}
}

window.addEventListener('wheel', function() {
	hideMyTraces();
});

function hideMyTraces() {
	//Show sub button on homepage recommendations and remove from sidebar
	enumerateTags("a", function(elem) {
		if(elem.title === "PewDiePie" && (elem.href.indexOf("/user/PewDiePie") !== -1 || elem.href.indexOf("/channel/UC-lHJZR3Gqxm24_Vd_AJ5Yw") !== -1) && !elem.__alreadyDisguised) {
			console.log("Something is missing from here. Quickly! Before he sees!");
			let subBtn = elem.parentElement.parentElement.parentElement.querySelector("#subscribe-button");
			if(subBtn) {
				subBtn.innerHTML = `<ytd-subscribe-button-renderer class="style-scope ytd-shelf-renderer">
				    <paper-button noink="" class="style-scope ytd-subscribe-button-renderer" role="button" tabindex="0" animated="" aria-disabled="false" elevation="0" aria-label="Subscribe to PewDiePie.">
				      <yt-formatted-string class="style-scope ytd-subscribe-button-renderer">Subscribe <span class="deemphasize style-scope yt-formatted-string">83M</span></yt-formatted-string>
				    <paper-ripple class="style-scope paper-button">
				    <div id="background" class="style-scope paper-ripple"></div>
				    <div id="waves" class="style-scope paper-ripple"></div>
				  </paper-ripple></paper-button>
				    <div id="notification-preference-toggle-button" class="style-scope ytd-subscribe-button-renderer" hidden=""><ytd-toggle-button-renderer button-renderer="" class="style-scope ytd-subscribe-button-renderer style-grey-text" is-icon-button="" has-no-text=""><a class="yt-simple-endpoint style-scope ytd-toggle-button-renderer" tabindex="-1"><yt-icon-button id="button" class="style-scope ytd-toggle-button-renderer style-grey-text" aria-pressed="false"><button id="button" class="style-scope yt-icon-button" aria-label="Current setting is occasional notifications. Tap to change your notification setting for Electronic Music - Topic"><yt-icon class="style-scope ytd-toggle-button-renderer"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope yt-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope yt-icon">
				        <path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z" class="style-scope yt-icon"></path>
				      </g></svg>
				  </yt-icon></button></yt-icon-button></a></ytd-toggle-button-renderer></div>
				  </ytd-subscribe-button-renderer>`;
				let buttons = subBtn.querySelectorAll('.style-scope.ytd-subscribe-button-renderer');
				buttons[0].style.backgroundColor = "var(--yt-spec-brand-button-background)";
				buttons[0].style.color = "var(--yt-subscribe-button-text-color)";
				buttons[1].textContent = "SUBSCRIBE 83M";
				elem.__alreadyDisguised = true;
			} else {
				//Probaly a sidebar
				console.log("Whoops! Gotta hide the body!");
				elem.parentElement.parentElement.removeChild(elem.parentElement);
			}
		}
	});
}

function enumerateTags(tagName, callback) {
	var tags = document.getElementsByTagName(tagName);
	for(var i=0, tag; tag = tags[i]; i++) {
		if(callback(tag) === true) break;
	}
}

function eventFire(el, etype){
	if (el.fireEvent) {
		el.fireEvent('on' + etype);
	} else {
		var evObj = document.createEvent('Events');
		evObj.initEvent(etype, true, false);
		el.dispatchEvent(evObj);
	}
}